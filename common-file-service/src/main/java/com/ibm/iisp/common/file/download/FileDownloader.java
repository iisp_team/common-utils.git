package com.ibm.iisp.common.file.download;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface FileDownloader {
	public void download(String path, HttpServletRequest request, HttpServletResponse resp) throws IOException;
}
