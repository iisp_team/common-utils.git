package com.ibm.iisp.common.file.download;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

/**
 * 用apache/ngix的x-sendfile模块下载文件
 * 
 * @author JohnnyZhou
 *
 */
public class XSendFileDownloader implements FileDownloader {
	Logger log = LoggerFactory.getLogger(getClass());
	@Override
	public void download(String path, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String relaPath = path.startsWith("/") ? path.substring(1) : path;
		log.debug("X-Sendfile:{}", relaPath);
		response.setHeader("X-Sendfile", relaPath);
		// response.setHeader("X-Accel-Charset", "utf-8");
		response.setStatus(HttpStatus.OK.value());
	}

}
