/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.file.util;

/**
 * 类作用：与客户的文档系统接口
 * 
 * @author Johnny@cn.ibm.com 使用说明：
 */
public interface CustomDocSysHelper {
	/**
	 * @param uri
	 *            要上传到文档系统的文件URI
	 * @param oldUri
	 *            如果是更新操作，则要提供老的文件URI
	 * @param isOverride
	 *            仅在更新操作时生效,指定是不是要覆盖老版本，如果选否，则老文件不删除，新文件保存为新版本
	 * @return 文档系统中，取回文件的URI,如存储路径或文件ID等
	 */
	String uploadFile(String uri, String oldUri, boolean isOverride);

	/**
	 * @param uri
	 *            文档系统中
	 * @return 保存在本地磁盘中的文件
	 */
	String downloadFile(String uri);
}
