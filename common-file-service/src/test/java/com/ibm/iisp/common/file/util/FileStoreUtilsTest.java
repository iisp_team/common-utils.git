/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.file.util;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 类作用：
 * @author Johnny@cn.ibm.com
 * 使用说明：
 */
public class FileStoreUtilsTest {
	static FileStoreUtils fu = new FileStoreUtils();
	@BeforeClass
	public static void init(){
		// fu.fileStorageRoot=System.getenv("TEMP");
	}
	@Test
	public void testTxt() throws IOException {
		fu.uploadTxtFile("/p1/p2/p3/t.txt", "4");
	}
	@Test
	public void testenc() throws IOException {
		String fn = (fu.encodeFileName("/rpt00109300000.htm"));
		System.out.println(fn);
		// Assert.assertTrue(fn.startsWith("/rpt00109300000") && fn.endsWith(".htm"));
		fn = (fu.encodeFileName("/c:/test/rpt00109300000.htm"));
		System.out.println(fn);
		// Assert.assertTrue( fn.startsWith("/c:/test/rpt00109300000") &&
		// fn.endsWith(".htm"));
		fn = (fu.encodeFileName("rpt00109300000.htm"));
		System.out.println(fn);
		// Assert.assertTrue(fn.startsWith("rpt00109300000") && fn.endsWith(".htm"));
	}

}
