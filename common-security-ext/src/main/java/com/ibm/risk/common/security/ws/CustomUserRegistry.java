/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.security.ws;

import java.rmi.RemoteException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.ibm.websphere.security.CertificateMapFailedException;
import com.ibm.websphere.security.CertificateMapNotSupportedException;
import com.ibm.websphere.security.CustomRegistryException;
import com.ibm.websphere.security.EntryNotFoundException;
import com.ibm.websphere.security.NotImplementedException;
import com.ibm.websphere.security.PasswordCheckFailedException;
import com.ibm.websphere.security.Result;
import com.ibm.websphere.security.UserRegistry;
import com.ibm.websphere.security.cred.WSCredential;

/**
 * 类作用：
 * 
 * @author Johnny@cn.ibm.com 使用说明：
 */
public class CustomUserRegistry implements UserRegistry {


	Logger log = Logger.getLogger(this.getClass().getName());
	/**
	 * 
	 */
	private static final String GROUP_ID = "IRMP";

	// Logger log = LoggerFactory.getLogger(this.getClass());

	private String dataSource;
	DBPasswordChecker pcheck;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#checkPassword(java.lang.String, java.lang.String)
	 */
	@Override
	public String checkPassword(String username, String password)
		throws PasswordCheckFailedException, CustomRegistryException, RemoteException {
		return pcheck.checkPassword(username, password);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#createCredential(java.lang.String)
	 */
	@Override
	public WSCredential createCredential(String userSecurityName)
		throws NotImplementedException, EntryNotFoundException, CustomRegistryException, RemoteException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getGroupDisplayName(java.lang.String)
	 */
	@Override
	public String getGroupDisplayName(String groupSecurityName) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		return groupSecurityName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getGroupSecurityName(java.lang.String)
	 */
	@Override
	public String getGroupSecurityName(String uniqueGroupId) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		return uniqueGroupId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getGroups(java.lang.String, int)
	 */
	@Override
	public Result getGroups(String pattern, int limit) throws CustomRegistryException, RemoteException {
		Result rst = new Result();
		ArrayList<String> g = new ArrayList<>();
		g.add(GROUP_ID);
		rst.setList(g);
		return rst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getGroupsForUser(java.lang.String)
	 */
	@Override
	public List<String> getGroupsForUser(String arg0) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		ArrayList<String> g = new ArrayList<>();
		g.add(GROUP_ID);
		return g;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getRealm()
	 */
	@Override
	public String getRealm() throws CustomRegistryException, RemoteException {
		return "IISP_Realm";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getUniqueGroupId(java.lang.String)
	 */
	@Override
	public String getUniqueGroupId(String groupSecurityName) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		return groupSecurityName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getUniqueGroupIds(java.lang.String)
	 */
	@Override
	public List<String> getUniqueGroupIds(String uniqueUserId) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		ArrayList<String> g = new ArrayList<>();
		g.add(GROUP_ID);
		return g;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getUniqueUserId(java.lang.String)
	 */
	@Override
	public String getUniqueUserId(String userSecurityName) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		return userSecurityName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getUserDisplayName(java.lang.String)
	 */
	@Override
	public String getUserDisplayName(String userSecurityName) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		return userSecurityName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getUserSecurityName(java.lang.String)
	 */
	@Override
	public String getUserSecurityName(String uniqueUserId) throws EntryNotFoundException, CustomRegistryException, RemoteException {
		return uniqueUserId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getUsers(java.lang.String, int)
	 */
	@Override
	public Result getUsers(String pattern, int limit) throws CustomRegistryException, RemoteException {
		ArrayList<String> us = new ArrayList<>();
		us.add("WASADMIN");
		Result rst = new Result();
		rst.setList(us);
		return rst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#getUsersForGroup(java.lang.String, int)
	 */
	@Override
	public Result getUsersForGroup(String pattern, int arg1)
		throws NotImplementedException, EntryNotFoundException, CustomRegistryException, RemoteException {
		ArrayList<String> us = new ArrayList<>();
		us.add("WASADMIN");
		Result rst = new Result();
		rst.setList(us);
		return rst;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#initialize(java.util.Properties)
	 */
	@Override
	public void initialize(Properties props) throws CustomRegistryException, RemoteException {
		this.dataSource = props.getProperty("dataSource");
		pcheck = new DBPasswordChecker(dataSource);
		log.info("DataSource:" + dataSource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#isValidGroup(java.lang.String)
	 */
	@Override
	public boolean isValidGroup(String arg0) throws CustomRegistryException, RemoteException {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#isValidUser(java.lang.String)
	 */
	@Override
	public boolean isValidUser(String arg0) throws CustomRegistryException, RemoteException {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.websphere.security.UserRegistry#mapCertificate(java.security.cert.X509Certificate[])
	 */
	@Override
	public String mapCertificate(X509Certificate[] arg0)
		throws CertificateMapNotSupportedException, CertificateMapFailedException, CustomRegistryException, RemoteException {
		return null;
	}

}
