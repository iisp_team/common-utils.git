/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.security.ws;

import static org.junit.Assert.assertEquals;

import java.rmi.RemoteException;

import org.testng.annotations.Test;

import com.ibm.websphere.security.PasswordCheckFailedException;

/**
 * 类作用：
 * 
 * @author Johnny@cn.ibm.com 使用说明：
 */
public class CustomUserRegistryTest {
	class DbPasscheckerEmptyImpl extends DBPasswordChecker {

		/**
		 * @param dataSourceName
		 */
		public DbPasscheckerEmptyImpl(String dataSourceName) {
			super(dataSourceName);
		}

		@Override
		protected String getDbPassword(String username) throws RemoteException {
			if ("a".equals(username)) {
				return "";
			}
			if ("b".equals(username)) {
				return null;
			}
			return SecurityUtils.MD5(username);
		}

	}

	@Test
	public void testPasscheck() throws Exception {
		DbPasscheckerEmptyImpl cu = new DbPasscheckerEmptyImpl("");
		String user = cu.checkPassword("admin", "admin");
		assertEquals("admin", user);
		user = cu.checkPassword("a", "");
		assertEquals("a", user);
		user = cu.checkPassword("admin", "admin");

	}

	@Test(expectedExceptions = PasswordCheckFailedException.class)
	public void testPasscheck1() throws Exception {
		DbPasscheckerEmptyImpl cu = new DbPasscheckerEmptyImpl("");
		try {
			cu.checkPassword("admin", "admin1");
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw e;
		}

	}

	@Test(expectedExceptions = PasswordCheckFailedException.class)
	public void testPasscheck2() throws Exception {
		DbPasscheckerEmptyImpl cu = new DbPasscheckerEmptyImpl("");
		try {
			cu.checkPassword("b", "b");
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw e;
		}

	}

}
