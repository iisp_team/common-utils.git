package com.ibm.iisp.common.dao;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.QueryHints;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@QueryHints({ @QueryHint(name = "org.hibernate.cacheable", value = "true") })
public @interface CachedQuery {

}
