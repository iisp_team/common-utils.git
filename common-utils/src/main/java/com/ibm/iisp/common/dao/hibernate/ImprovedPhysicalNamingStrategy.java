/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.dao.hibernate;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

import com.ibm.iisp.common.util.StringUtilsExt;

/**
 * 类作用：由于Hiberate5 移除了ImprovedNamingStrategy, 自己实现一个替代，生成“A_B”格式的名称
 * @author Johnny@cn.ibm.com
 * 使用说明：
 */
public class ImprovedPhysicalNamingStrategy implements PhysicalNamingStrategy {

    @Override
    public Identifier toPhysicalCatalogName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalColumnName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalSchemaName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalSequenceName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    @Override
    public Identifier toPhysicalTableName(Identifier identifier, JdbcEnvironment jdbcEnv) {
        return convert(identifier);
    }

    Identifier convert(Identifier identifier) {
 		if (identifier == null || StringUtils.isBlank(identifier.getText())) {
            return identifier;
        }

       String newName = StringUtilsExt.toUnderscopeSeperated(identifier.getText());
        return Identifier.toIdentifier(newName);
    }

}