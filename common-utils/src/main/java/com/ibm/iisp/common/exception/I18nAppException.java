package com.ibm.iisp.common.exception;

/**
 * IISP的基础异常类，支持国际化消息显示。
 * 
 * @author Johnny
 *
 */
public class I18nAppException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6949943872655495660L;
	/**
	 * 对应i18n properties文件中的key, 如IISP_AUTH_ROLE_001 注意，对应的properties文件中的key要加上"err."前缀，如：err.IISP_AUTH_ROLE_001
	 */
	private String errorCode;
	private Object[] args;
	public I18nAppException() {
	}

	/**
	 * @param errorCode
	 *            对应语言 properties文件中的key, 如IISP_AUTH_ROLE_001。<br>
	 *            注意，对应的properties文件中的key要加上"err."前缀，如：err.IISP_AUTH_ROLE_001
	 * @param args
	 *            properties文件中的消息的参数，如：“保存新角色[{0}]出错!”，那么args[0]将替换消息体中的{0}。
	 * @param defaultMessage
	 *            如果没有找到语言 properties文件中的相应的key时，用这个缺省消息来显示
	 */
	public I18nAppException(String errorCode, Object[] args, String defaultMessage) {
		super(defaultMessage);
		this.args = args;
		this.errorCode = errorCode;
	}

	/**
	 * @param errorCode
	 *            对应语言 properties文件中的key, 如IISP_AUTH_ROLE_001。<br>
	 *            注意，对应的properties文件中的key要加上"err."前缀，如：err.IISP_AUTH_ROLE_001
	 * @param args
	 *            properties文件中的消息的参数，如：“保存新角色[{0}]出错!”，那么args[0]将替换消息体中的{0}。
	 * @param defaultMessage
	 *            如果没有找到语言 properties文件中的相应的key时，用这个缺省消息来显示
	 * @param cause
	 *            底层抓获的异常
	 */
	public I18nAppException(String errorCode, Object[] args, String defaultMessage, Throwable cause) {
		super(defaultMessage, cause);
		this.args = args;
		this.errorCode = errorCode;
	}


	/**
	 * @return the errorCode, also used to get localized message from resources. the key format is "err.errorCode".
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return i18n message key.
	 */
	public String getMessageKey() {
		return "err." + errorCode;
	}

	/**
	 * @return the args to format the i18n message
	 */
	public Object[] getArgs() {
		return args;
	}

	/**
	 * @param args the args to set
	 */
	public void setArgs(Object[] args) {
		this.args = args;
	}

}
