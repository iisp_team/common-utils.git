package com.ibm.iisp.common.spring;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

public class CustomPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
	private static Map<String, String> propertiesData = new HashMap<String, String>();

	@Override
	protected String convertProperty(String propertyName, String propertyValue) {
		String value = super.convertProperty(propertyName, propertyValue);

		propertiesData.put(propertyName, value);

		return value;
	}

	/**
	 * 获取Properties文件中的值,如果不存在则返回defaultValue
	 *
	 * @param key          key
	 * @param defaultValue default value
	 * @return value
	 */
	public static String getProperty(String key, String defaultValue) {
		String value = propertiesData.get(key);
		return value == null ? defaultValue : value;
	}

	/**
	 * 获取Properties文件中的值,不存在返回null
	 *
	 * @param key key
	 * @return value
	 */
	public static String getProperty(String key) {
		return getProperty(key, null);
	}

	public static Map<String, String> getPropertiesData() {
		return propertiesData;
	}

}
