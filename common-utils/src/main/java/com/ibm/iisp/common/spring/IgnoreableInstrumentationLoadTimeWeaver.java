/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.spring;

import java.lang.instrument.ClassFileTransformer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;

/**
 * 类作用：忽略eclipselink启动时的time-waver的报错
 *
 * @author Johnny@cn.ibm.com
 *
 */

public class IgnoreableInstrumentationLoadTimeWeaver extends InstrumentationLoadTimeWeaver {
	Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public void addTransformer(ClassFileTransformer transformer) {
		try {
			super.addTransformer(transformer);
		} catch (Exception e) {
			log.warn("Error to addTransformer:" + e.getMessage());
		}
	}
}
