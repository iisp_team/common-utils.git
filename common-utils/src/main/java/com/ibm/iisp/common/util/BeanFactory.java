package com.ibm.iisp.common.util;

import java.util.Map;

/**
 * @author Johnny
 * 
 */
public abstract class BeanFactory {
	static BeanFactory instance;

	/**
	 * @return instance.
	 */
	public static BeanFactory getInstance() {
		return instance;
	}

	/**
	 * @param <E> e.
	 * @param clazz clazz.
	 * @return e.
	 */
	public abstract <E> E getBean(Class<E> clazz);

	public abstract <E> Map<String, E> getBeans(Class<E> clazz);

	/**
	 * @param <E> e.
	 * @param clazz clazz.
	 * @param id id.
	 * @return e.
	 */
	public abstract <E> E getBean(Class<E> clazz, String id);

}
