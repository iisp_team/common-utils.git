package com.ibm.iisp.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonObjectMapperFactory {
	static ObjectMapper objectMapper = new ObjectMapper();

	public static ObjectMapper getObjectMapper() {
		return objectMapper;
	}
}
