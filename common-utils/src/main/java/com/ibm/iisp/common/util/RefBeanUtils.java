package com.ibm.iisp.common.util;

import java.util.ArrayList;
import java.util.List;

import com.ibm.iisp.common.vo.RefBean;

public class RefBeanUtils {
	public static List<RefBean> toRefBeanList(List<Object[]> datas) {
		ArrayList<RefBean> refbs = new ArrayList<>(datas.size());
		for (Object[] data : datas) {
			refbs.add(new RefBean((String) data[0], (String) data[1]));
		}
		return refbs;
	}
}
