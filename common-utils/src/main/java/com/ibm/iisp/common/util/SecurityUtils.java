package com.ibm.iisp.common.util;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
	/**
	 * 获取当前登录用户代码
	 * 
	 * @return 如果没有登录环境，就返回null
	 */
	public static String getCurrentUser() {
		String user = null;
		SecurityContext context = SecurityContextHolder.getContext();
		if (context != null) {
			Object principal = context.getAuthentication() == null ? null : context.getAuthentication().getPrincipal();
			if (principal != null) {
				user = principal.toString();
			}
		}
		return user;
	}
}
