package com.ibm.iisp.common.util;


import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Johnny
 * 
 */
@Component
public class SpringBeanFactory extends BeanFactory implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	/**
	 * 
	 */
	public SpringBeanFactory() {
		instance = this;
	}

	/** {@inheritDoc} */
	@Override
	public <E> E getBean(Class<E> clazz) {
		return applicationContext.getBean(clazz);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> E getBean(Class<E> clazz, String id) {

		return (E) applicationContext.getBean(id);
	}

	@Override
	public void setApplicationContext(ApplicationContext ac) throws BeansException {
		this.applicationContext = ac;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ibm.iisp.common.util.BeanFactory#getBeans(java.lang.Class)
	 */
	@Override
	public <E> Map<String,E> getBeans(Class<E> clazz) {
		return applicationContext.getBeansOfType(clazz);
	}
}
