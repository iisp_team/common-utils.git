/**
 *
 */
package com.ibm.iisp.common.vo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.ibm.iisp.common.dao.AuditListener;

/**
 * @author Johnny
 *
 */
@MappedSuperclass
@EntityListeners(AuditListener.class)
public abstract class AuditVO implements Serializable{
	/**
	 *
	 */
	private static final long serialVersionUID = -3470794757789839181L;
	private String createUser;
	@Column(name = "UPDATE_USER")
	private String lastUpdateUser;
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;
	@Basic
	@Version
	private Date lastUpdateTime;

	/**
	 * @return string.
	 */
	public String getCreateUser() {
		return createUser;
	}

	/**
	 * @param createUser
	 *            is to set value.
	 */
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	/**
	 * @return string.
	 */
	public String getLastUpdateUser() {
		return lastUpdateUser;
	}

	/**
	 * @param updateUser
	 *            is to set value.
	 */
	public void setLastUpdateUser(String updateUser) {
		this.lastUpdateUser = updateUser;
	}

	/**
	 * @return date.
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            is to set value.
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return date.
	 */
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	/**
	 * @param updateTime
	 *            is to set value.
	 */
	public void setLastUpdateTime(Date updateTime) {
		this.lastUpdateTime = updateTime;
	}
	public abstract Object getId();
}
