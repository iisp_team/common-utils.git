/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.vo;

/**
 * 类作用：用于需要访问Code的场合
 * 
 * @author Johnny@cn.ibm.com 使用说明：
 */
public interface ICodeBean {
	String getCode();
}
