package com.ibm.iisp.common.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

/**
 * 分页查询参数基类.
 *
 * @author Johnny
 *
 */
public abstract class PagedQueryParam implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -2198575204083560476L;
	private int pageNumber = 0;
	private int pageSize = 10;
	private String orderBy;
	/**
	 * 记录总数，null表示未知，需要查询时求出总数，非null值表示已经知道总数，无需再次求值
	 */
	private Integer recordCount;

	/**
	 *
	 */
	public void init() {
		pageNumber = 0;
		pageSize = 10;
		orderBy = null;
		recordCount = null;
	}

	/**
	 * 要查询的页码，从0开始.
	 *
	 * @return the pageNumber
	 */
	public int getPageNumber() {
		return pageNumber;
	}

	/**
	 * @param pageNumber 要查询的页码，从0开始.
	 */
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	/**
	 * 页面大小，缺省为每页10行.
	 *
	 * @return the pageSize
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 *
	 * 排序，分号分隔每一组条件，组内用冒号分隔。 属性引用，如: seq:ASC; userCode:DESC 表示 seq升序，然后userCode降序
	 * 支持SQL条件，如：SQL:abs(VALUE_DIFF):ASC;
	 * SQL:decode(checked_summary,0,1,abs(VALUE_DIFF/checked_summary)):DESC
	 * 
	 * @return string.
	 */
	public String getOrderBy() {
		return orderBy;
	}

	/**
	 * @param orderBy
	 *
	 *                排序，分号分隔每一组条件，组内用冒号分隔。 属性引用，如: seq:ASC; userCode:DESC 表示
	 *                seq升序，然后userCode降序 支持SQL条件，如：SQL:abs(VALUE_DIFF):ASC;
	 *                SQL:decode(checked_summary,0,1,abs(VALUE_DIFF/checked_summary)):DESC
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * @return the recordCount
	 */
	public Integer getRecordCount() {
		return recordCount;
	}

	/**
	 * @param recordCount the recordCount to set
	 */
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public PageRequest toPageRequest() {
		List<Order> sortOrders = null;
		if (this.getOrderBy() != null && this.getOrderBy().length() > 0) {
			// seq:ASC; userCode:DESC
			sortOrders = new ArrayList<>();
			String[] orders = this.getOrderBy().split(";");
			for (String order : orders) {
				if (order.contains(":")) {//
					String[] ordFactors = order.split(":");
					if ("ASC".equalsIgnoreCase(ordFactors[1])) {
						sortOrders.add(Order.asc(ordFactors[0].trim()));
					} else if ("DESC".equalsIgnoreCase(ordFactors[1])) {
						sortOrders.add(Order.desc(ordFactors[0].trim()));
					} else {
						throw new RuntimeException("无效排序表达式：" + order);
					}
				} else {
					sortOrders.add(Order.asc(order.trim()));
				}
			}
		}
		Sort sort = null;
		if (sortOrders != null && !sortOrders.isEmpty()) {
			sort = Sort.by(sortOrders);
		}
		return PageRequest.of(this.getPageNumber(), this.getPageSize(), sort);
	}

}
