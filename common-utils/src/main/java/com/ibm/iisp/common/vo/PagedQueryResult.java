package com.ibm.iisp.common.vo;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;

/**
 * 查询结果基类.
 *
 * @author Johnny
 *
 * @param <T>
 *            要返回的对象类型
 */
public class PagedQueryResult<T> implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 3326764337561112775L;

	private List<T> datas;

	private Boolean isLastPage;

	private Integer recordCount;


	private Integer pageCount;

	private int indexNumber;

	public PagedQueryResult(Page<T> page) {
		this.setDatas(page.getContent());
		this.setPageCount(page.getTotalPages());
		this.setRecordCount((int) page.getTotalElements());
		this.setIsLastPage(page.isLast());
	}
	/**
	 * @param totalDataCount
	 *            总数据件数
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNumber
	 *            当前的页数
	 */
	public PagedQueryResult(int totalDataCount, int pageSize, int pageNumber) {
		super();
		this.recordCount = totalDataCount;
		if (this.recordCount <= 0) {
			return;
		}
		// 总页数
		this.pageCount = (this.recordCount + pageSize - 1) / pageSize;
		// 是否为最后一页
		this.isLastPage = (pageNumber == this.pageCount ? true : false);
	}

	/**
	 *
	 */
	public PagedQueryResult() {
		super();
	}

	/**
	 * 返回的数据集.
	 *
	 * @return the datas
	 */
	public List<T> getDatas() {
		return datas;
	}

	/**
	 * @param datas
	 *            the datas to set
	 */
	public void setDatas(List<T> datas) {
		this.datas = datas;
	}

	/**
	 * 满足查询条件的总记录数， null 意味着未知。注：只在查询第一页时返回正确的总记录数，其它页码时，返回-1.
	 *
	 * @return the totalDataCount
	 */
	public Integer getRecordCount() {
		return recordCount;
	}

	/**
	 * @param totalDataCount
	 *            the totalDataCount to set
	 */
	public void setRecordCount(Integer totalDataCount) {
		this.recordCount = totalDataCount;
	}


	/**
	 * 满足查询条件的总页数， null 意味着未知。注：只在查询第一页时返回正确的总记录数，其它页码时，返回-1.
	 *
	 * @return the pageCount
	 */
	public Integer getPageCount() {
		return pageCount;
	}

	/**
	 * @param pageCount
	 *            the pageCount to set
	 */
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * 标志是否最后一页，True: 是最后一页，False: 不是，null：未知.
	 *
	 * @return the lastPage
	 */
	public Boolean getIsLastPage() {
		return isLastPage;
	}

	/**
	 * @param lastPage
	 *            the lastPage to set
	 */
	public void setIsLastPage(Boolean lastPage) {
		this.isLastPage = lastPage;
	}

	/**
	 * 计算开始数.
	 *
	 * @return the indexNumber
	 */
	public int getIndexNumber() {
		return indexNumber;
	}
}
