/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.vo;

import java.util.Collection;
import java.util.List;

/**
 * 类作用：
 * @author JohnnyZhou@cn.ibm.com
 * 使用说明：
 */
public class RefBeanTreeNode implements IRefBeanTree {
	private String code;
	private String name;
	private List<RefBeanTreeNode> children;

	/**
	 * 
	 */
	public RefBeanTreeNode() {
		super();
	}

	public RefBeanTreeNode(String code, String name, List<RefBeanTreeNode> children) {
		super();
		this.code = code;
		this.name = name;
		this.children = children;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(List<RefBeanTreeNode> children) {
		this.children = children;
	}

	/* (non-Javadoc)
	 * @see com.ibm.iisp.common.vo.IRefBeanTree#getCode()
	 */
	@Override
	public String getCode() {
		return code;
	}

	/* (non-Javadoc)
	 * @see com.ibm.iisp.common.vo.IRefBeanTree#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.ibm.iisp.common.vo.IRefBeanTree#getChildren()
	 */
	@Override
	public Collection<? extends IRefBeanTree> getChildren() {
		return children;
	}

}
