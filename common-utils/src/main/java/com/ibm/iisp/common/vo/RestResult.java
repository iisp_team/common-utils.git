/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.vo;

/**
 * 类作用：简单的Rest返回结果数据结构
 * 
 * @author Johnny@cn.ibm.com 使用说明：
 */
public class RestResult {
	private String status;
	private Object value;

	/**
	 * 
	 */
	public RestResult() {
		super();
		this.status = "OK";
	}

	/**
	 * @param value
	 *            value
	 */
	public RestResult(Object value) {
		super();
		this.status = "OK";
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "RestResult [status=" + status + ", value=" + value + "]";
	}

}
