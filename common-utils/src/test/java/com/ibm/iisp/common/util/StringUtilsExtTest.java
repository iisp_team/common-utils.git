/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.util;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

import com.ibm.iisp.common.util.StringUtilsExt;

/**
 * 类作用：
 * @author Johnny@cn.ibm.com
 * 使用说明：
 */
public class StringUtilsExtTest {
	
	@Test
	public void testReplaceParameters(){
		HashMap<String, String> params = new HashMap<>();
		params.put("dataDateFrom", "20110930");
		params.put("dataDateTo", "20120930");
		params.put("nodeCode", "0000");
		String source = "/test?dataDateFrom=${dataDateFrom}&dataDateTo=${dataDateTo}&nodeCode=${nodeCode}&123=3";
		source = StringUtilsExt.replacePlaceHolders(source , params);
		System.out.println(source);
		Assert.assertEquals("/test?dataDateFrom=20110930&dataDateTo=20120930&nodeCode=0000&123=3", source);
	}
	@Test
	public void testReplaceParameters2(){
		HashMap<String, String> params = new HashMap<>();
		params.put("dataDateFrom", "20110930");
		params.put("dataDateTo", "20120930");
		//params.put("nodeCode", "0000");
		String source = "/test?dataDateFrom=${dataDateFrom}&dataDateTo=${dataDateTo}&nodeCode=${nodeCode}&123=3";
		source = StringUtilsExt.replacePlaceHolders(source , params);
		System.out.println(source);
		Assert.assertEquals("/test?dataDateFrom=20110930&dataDateTo=20120930&nodeCode=${nodeCode}&123=3", source);
	}
	@Test
	public void testUnderScope(){
		String newName = StringUtilsExt.toUnderscopeSeperated("dataDateOld1");
		Assert.assertEquals(newName, "DATA_DATE_OLD1");
		
		newName = StringUtilsExt.toUnderscopeSeperated("dataURLoldI2");
		Assert.assertEquals(newName, "DATA_URLOLD_I2");
	}
	
	@Test
	public void testTrimToEncloseTag(){
		String html = " <table> <tr> <td> </td></tr> </table> </tr> </div> </html>";
		html = StringUtilsExt.trimToEncloseTag(html);
		Assert.assertEquals(html, "<table> <tr> <td> </td></tr> </table>");
		html = " <table	a='b'> <tr> <td><table></table> </td></tr> </table> ddd </tr> ddd</div> </html>";
		html = StringUtilsExt.trimToEncloseTag(html);
		Assert.assertEquals(html, "<table	a='b'> <tr> <td><table></table> </td></tr> </table>");
	}

	@Test
	public void testEncodeUni() {
		String uStr = StringUtilsExt.escapeJs("大ee周?%#@$=");
		System.out.println(uStr);
		System.out.println(StringUtilsExt.unescapeJs(uStr));

	}
}
