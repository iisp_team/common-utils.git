/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.rest;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.cfg.ConfigFeature;

/**
 * 类作用：用于Spring容器中配置缺省的ObjectMapper
 * 
 * @author Johnny@cn.ibm.com
 */
public class JacksonObjectMapper extends ObjectMapper {
	/**
	 * 
	 */
	private static final long serialVersionUID = 967265536811151073L;
	Logger log = LoggerFactory.getLogger(getClass());

	public JacksonObjectMapper(Map<ConfigFeature, Boolean> features) {
		for (Map.Entry<ConfigFeature, Boolean> e : features.entrySet()) {
			ConfigFeature f = e.getKey();
			if (f instanceof DeserializationFeature) {
				this.configure((DeserializationFeature) f, e.getValue());
			} else if (f instanceof SerializationFeature) {
				this.configure((SerializationFeature) f, e.getValue());
			} else if (f instanceof MapperFeature) {
				this.configure((MapperFeature) f, e.getValue());
			} else {
				log.error("Unkown Feature:" + f);
			}

		}
	}
	public void setModules(List<com.fasterxml.jackson.databind.Module> modules) {
		this.registerModules(modules);
	}
}
