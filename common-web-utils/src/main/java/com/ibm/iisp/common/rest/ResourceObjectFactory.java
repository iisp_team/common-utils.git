/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.rest;

import java.util.HashSet;
import java.util.Set;

//import javax.ws.rs.ext.Provider;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 类作用：用于获取Spring容器中配置的resources
 * 
 * @author Johnny@cn.ibm.com
 */
public class ResourceObjectFactory implements FactoryBean<Set<Object>>, ApplicationContextAware {
	private Set<Object> resources;
	private ApplicationContext appContext;
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		this.appContext = appContext;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public Set<Object> getObject() throws Exception {
		if (resources == null) {
			resources = new HashSet<>();
			resources.addAll(appContext.getBeansWithAnnotation(RestService.class).values());
			// resources.addAll(appContext.getBeansWithAnnotation(Provider.class).values());
		}
		return resources;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	@Override
	public Class<?> getObjectType() {
		return Set.class;
	}

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	@Override
	public boolean isSingleton() {
		return true;
	}

}
