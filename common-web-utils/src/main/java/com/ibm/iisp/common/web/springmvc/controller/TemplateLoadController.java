/**
 * 
 */
package com.ibm.iisp.common.web.springmvc.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Johnny
 * 用于获取指定的template.
 * 如：/auth/roles.tpl.ftl
 *
 */
@RequestMapping("/templates")
public class TemplateLoadController {
	private boolean cacheable = false;
	@RequestMapping(value="/**", method = RequestMethod.GET)
	public String getTemplate(HttpServletRequest request, HttpServletResponse response) {
		if (cacheable) {
			long ctime = new Date().getTime();
			response.setDateHeader("Last-Modified",ctime);
		    //Expires:过时期限值 
		    response.setDateHeader("Expires", ctime + 20000);
		    //Cache-Control来控制页面的缓存与否,public:浏览器和缓存服务器都可以缓存页面信息；
		    response.setHeader("Cache-Control", "public");
		    //Pragma:设置页面是否缓存，为Pragma则缓存，no-cache则不缓存
		    response.setHeader("Pragma", "Pragma"); 
		}
		
		String path = request.getServletPath();
		path = path.substring("/templates".length(), path.length() - 4);
		return path;
	}

	/**
	 * @param envName the envName to set
	 */
	@Value("${envName}") public void setEnvName(String envName) {
		cacheable = "UAT".equals(envName) || "PROD".equals(envName);
	}
}
