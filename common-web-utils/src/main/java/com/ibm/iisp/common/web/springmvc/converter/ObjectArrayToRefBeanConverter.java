package com.ibm.iisp.common.web.springmvc.converter;

import org.springframework.core.convert.converter.Converter;

import com.ibm.iisp.common.vo.RefBean;

public class ObjectArrayToRefBeanConverter implements Converter<Object[], RefBean> {

	@Override
	public RefBean convert(Object[] source) {
		if (source != null) {
			if (source.length != 2) {
				throw new RuntimeException("数级长度只能为2,实际长度:" + source.length);
			}
			return new RefBean((String) source[0], (String) source[1]);
		}
		return null;
	}

}
