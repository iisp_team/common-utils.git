/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.web.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.ibm.iisp.common.vo.IRefBeanTree;
import com.ibm.iisp.common.vo.RefBeanTreeNode;

/**
 * 类作用：
 *
 * @author Johnny@cn.ibm.com 使用说明：
 */
public class TreeUtil {
	public static interface TreeNodePropertySetter<T extends IRefBeanTree> {
		void setTreeNodeProperty(RefBeanTreeNode node, T bean);
	}

	public static <T extends IRefBeanTree> List<RefBeanTreeNode> toTreeNodes(Collection<? extends IRefBeanTree> collection) {
		return toTreeNodes(collection, null);
	}


	public static <T extends IRefBeanTree> List<RefBeanTreeNode> toTreeNodes(Collection<T> refBeans,
		TreeNodePropertySetter<T> treeNodePropertySetter) {
		ArrayList<RefBeanTreeNode> nodes = new ArrayList<RefBeanTreeNode>(refBeans.size());
		for (T refBean : refBeans) {
			RefBeanTreeNode node = new RefBeanTreeNode();
			nodes.add(node);
			node.setCode(refBean.getCode());
			node.setName(refBean.getName());
			// node.setCss(org.isVirtual() ? "virtualOrg" : "organization");
			if (treeNodePropertySetter != null)
				treeNodePropertySetter.setTreeNodeProperty(node, refBean);
			if (refBean.getChildren() != null && !refBean.getChildren().isEmpty()) {
				@SuppressWarnings("unchecked")
				Collection<T> children = (Collection<T>) refBean.getChildren();
				node.setChildren(toTreeNodes(children, treeNodePropertySetter));
			}
		}
		return nodes;

	}
}
