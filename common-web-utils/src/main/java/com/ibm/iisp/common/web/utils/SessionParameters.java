/*
 * IBM Corporation.
 * Copyright (c) 2014 All Rights Reserved.
 */

package com.ibm.iisp.common.web.utils;

/**
 * 类作用：常数类，用于定义Session中的变量名称
 * 
 * @author Johnny@cn.ibm.com 使用说明：
 */
public interface SessionParameters {
	public final static String CURRENT_DATA_DATE = "currentDataDate";
	public final static String CURRENT_NODE_CODE = "currentNodeCode";
	public final static String CURRENT_NODE_NAME = "currentNodeName";
	public final static String CURRENT_USER_NAME = "currentUserName";
	public final static String CURRENT_USER_CODE = "currentUserCode";

}
