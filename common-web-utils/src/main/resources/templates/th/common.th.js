	var contextPath = [[${contextPath}]]; //base url
	var restRoot = contextPath + "/rest";
	var isRunning = 0;//用于记录ajax请求数
	var lastActiveTime = new Date(); //用于记录最后活动时间
	var fileDownloadRoot = [[${fileDownloadRoot}]];
	var envName=[[${envName}]];
	var prodMode = (envName == 'PROD' || envName == 'UAT');
	var langMap =  [[${mLabels}]];
	var calLang = eval('('+ langMap['calendar.labels'] + ')');
	if (!window.console) {
		var names = [ "log", "debug", "info", "warn", "error", "assert", "dir", "dirxml", "group", "groupEnd", "time", "timeEnd", "count",
		        "trace", "profile", "profileEnd" ];

		window.console = {};
		for (var i = 0; i < names.length; ++i)
			window.console[names[i]] = function(msg) {
			}
	}
	var csHandler = self.setInterval("checkSession()",60000);
	function checkSession() {
		if ((new Date()).getTime() - lastActiveTime.getTime() > [[${sessionTime}]]*60*1000) {
			csHandler = window.clearInterval(csHandler);
			window.location.href= contextPath + "/logout.html";
		}
	}