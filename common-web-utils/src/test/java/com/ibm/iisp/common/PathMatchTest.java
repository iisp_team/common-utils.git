package com.ibm.iisp.common;

import java.io.IOException;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.testng.annotations.Test;

public class PathMatchTest {

	@Test
	public void testPath() throws IOException {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		Resource[] resources = resolver.getResources("classpath*:i18n/*/messages.properties");
		for (Resource resource : resources) {
			System.out.println("Handling resource: {}..." + resource.getFilename());
		}
	}
}
