package com.ibm.iisp.common.datasource;

/*
 * Copyright 2002-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.sql.DataSource;

/**
 * 共享数据库连接。只读事务下，不提交事务
 *
 * @author WeiZhou
 *
 */
@SuppressWarnings("serial")
public class DynaDataSourceTransactionManager extends DataSourceTransactionManager {

    public DynaDataSourceTransactionManager() {
        super();
    }

    public DynaDataSourceTransactionManager(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected void doBegin(Object transaction, TransactionDefinition definition) {
        TransactionSynchronizationManager.setCurrentTransactionReadOnly(definition.isReadOnly());
        super.doBegin(transaction, definition);
    }

}
